<!DOCTYPE html>
<html>
<head>
  <title>Baza rejestracji uzytkownikow</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular.min.js"></script>
  <script src="zal.js"></script>
</head>
<body>
<!--Obsługa DB-->
<?php
  if(!isset($_COOKIE["key"])) //ciastko challenge key
  {
    $klucz = rand(1000, 9999);
    setcookie("key", $klucz, time() + (86400), "/"); //86400 = 1 dzien
    $challengekey = $_COOKIE["key"];
  }
  else
  {
    $challengekey = $_COOKIE["key"];
  }
  if(isset($_POST['rej']))
  {
    $pass = md5($_POST['pass']);
    $db = new PDO('sqlite:baza.db');
    $sql="INSERT INTO user (Imie, Nazwisko, Login, Haslo) VALUES(:imie, :nazwisko, :login, :haslo)";
    $res=$db->prepare($sql);
    $res->bindParam(':imie',$_POST['imie'],PDO::PARAM_STR, 160);
    $res->bindParam(':nazwisko',$_POST['nazwisko'],PDO::PARAM_STR, 160);
    $res->bindParam(':login',$_POST['login'],PDO::PARAM_STR, 160);
    $res->bindParam(':haslo',$pass,PDO::PARAM_STR, 160);
    $res->execute();
    }
  if(isset($_POST['log']))
  {
    $db = new PDO('sqlite:baza.db');
    $login = $_POST['login'];
    $sql="SELECT Haslo FROM user where Login = '$login'";
    $res=$db->query($sql);
    $rek=$res->fetch();
    $pass=$rek['Haslo'];

    print $_POST['log']. ' ';
    print $challengekey;

    if(md5($challengekey + $pass) == md5($_POST['log'] + md5($_POST['pass'])))
    {
      print ' ZALOGOWANO!';
    }
  }
?>
<!--Koniec DB-->
  <div class="container">
    <div class=page-header><h1>Technologie Webowe - zaliczenie<br />
      <small>Baza rejestracji użytkowników</small></h1></div>
  <div class="col-xs-6">
  <div class="panel panel-primary">
  <div class="panel-heading" >
    <h3>Rejestracja</h3>
  <div id=rejsestracja class="panel-body"/>
    <form action="" method="post" class="form">
      <input type="hidden" name="rej" value="true"/>
      <input type="text" id="Rlogin" name="login" class="form-control" placeholder="Login" required/><br />
      <input type="text" id="Rimie" name="imie" class="form-control" placeholder="Imię" required/><br />
      <input type="text" id="Rnazwisko" name="nazwisko" class="form-control" placeholder="Nazwisko" required/><br />
      <input type="password" id="Rpass" name="pass" class="form-control" placeholder="Hasło" required/><br />
      <button type="submit" class="btn btn-info">Utwórz konto</button>
    </form>
  </div><!--Rejestracja-->
  </div>
  </div>
</div>
<div class="col-xs-6">
<div class="panel panel-primary">
<div class="panel-heading">
  <h3>Logowanie</h3>
<div id=logowanie class="panel-body"/>
  <form action="" method="post">
    <input type="hidden" name="log" value="<?php print $challengekey ?>"/>
    <input type="text" id="login" name="login" class="form-control" placeholder="Login" required/><br />
    <input type="password" id="pass" name="pass" class="form-control" placeholder="Hasło" required/><br />
    <button type="submit" class="btn btn-info">Zaloguj</button>
  </form>
</div><!--Logowanie-->
</div>
</div>
  Liczba zarejestrowanych użytkowników <span class="badge" id="users"></span>
</div>
  <div class="col-xs-12">
  <div ng-app="zalapp" ng-controller="zalctrl">
    <div><h4>Lista użytkowników</h4></div>
    <div id=tabelaUserow>
      <table class="table">
      <thead>
        <tr><th>Imię</th><th>Nazwisko</th></tr>
      </thead>
      <tbody>
        <tr ng-repeat="x in imiona">
          <td>{{x.Imie}}</td><td>{{x.Nazwisko}}</td></tr>
      </tbody>
    </table>
    </div><!--TabelaUserow-->
  </div>
</div>
<script src="zalctrl.js"></script>
</body>
</html>
